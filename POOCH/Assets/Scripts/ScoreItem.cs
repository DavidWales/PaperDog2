﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreItem : MonoBehaviour
{
    public int points = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            SoundManager.PlaySound(SoundManager.SoundName.SCORE_COLLECTED);
            ScoreManager.ChangeScore(points);
            Destroy(this.gameObject);
        }
    }
}