﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour
{
    public TextTick score;

    void Start()
    {
        score.TickTo(0, ScoreManager.GetScore());
    }

    public static void PurchaseItem(StoreItem item)
    {
        if (ScoreManager.GetScore() >= item.cost && item.purchased == false)
        {
            int previousScore = ScoreManager.GetScore();
            ScoreManager.ChangeScore(-item.cost);
            item.Unlock();
            _inst.score.TickTo(previousScore, ScoreManager.GetScore());
        }
    }

    public void ExitStore()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        GameSaver.SaveVariables();
        ImageFadeInOut[] iFaders = transform.parent.GetComponentsInChildren<ImageFadeInOut>();
        foreach (ImageFadeInOut fader in iFaders)
        {
            fader.FadeOut();
        }

        TextFadeInOut[] tFaders = transform.parent.GetComponentsInChildren<TextFadeInOut>();
        foreach(TextFadeInOut fader in tFaders)
        {
            fader.FadeOut();
        }

        StartCoroutine(Destruct(iFaders, tFaders));
    }

    private bool AllFaded(ImageFadeInOut[] iFaders, TextFadeInOut[] tFaders)
    {
        foreach (ImageFadeInOut fader in iFaders)
        {
            if (fader.GetComponent<Image>().color.a > 0.0f)
            {
                return false;
            }
        }

        foreach (TextFadeInOut fader in tFaders)
        {
            if (fader.GetComponent<Text>().color.a > 0.0f)
            {
                return false;
            }
        }

        return true;
    }

    private IEnumerator Destruct(ImageFadeInOut[] iFaders, TextFadeInOut[] tFaders)
    {
        yield return new WaitUntil(() => AllFaded(iFaders, tFaders));
        Destroy(this.transform.parent.gameObject);
    }

    //Singleton Stuff
    private static StoreManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
    }
}