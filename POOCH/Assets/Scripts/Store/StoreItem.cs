﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreItem : MonoBehaviour
{
    public int cost = 1;
    public Sprite PurchasedImage;
    public string itemName;
    private Image image;
    public bool purchased = false;

    void Start()
    {
        image = this.GetComponent<Image>();
        GameSaver.GetItem(itemName);
        if(GlobalVariables.unlockedItems[itemName] == true)
        {
            Unlock();
        }
    }

    public void Purchase()
    {
        StoreManager.PurchaseItem(this);
    }

    virtual public void Unlock()
    {
        purchased = true;
        if (image && PurchasedImage)
        {
            image.sprite = PurchasedImage; //Change icon to purchased icon
        }
        GlobalVariables.unlockedItems[itemName] = true;
        //Unlock specific item in child class
    }
}