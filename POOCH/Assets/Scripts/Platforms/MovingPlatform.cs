﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float MoveSpeed = 1.0f;
    public Vector3 endPosition;
    private Vector3 startPosition;
    private bool moveTowardsEnd = true;
    private float distance = 0.0f;
    private float distanceTraveled = 0.0f;

    void Awake()
    {
        startPosition = this.transform.localPosition;
        distance = Vector3.Distance(startPosition, endPosition);
    }

    void Update()
    {
        Vector3 dir = (moveTowardsEnd) ? endPosition - this.transform.localPosition : startPosition - this.transform.localPosition;
        dir.Normalize();

        Vector3 movement = dir * MoveSpeed * Time.deltaTime;
        this.transform.localPosition += movement;
        distanceTraveled += movement.magnitude;
        if(Mathf.Abs(distanceTraveled - distance) <= 0.1f)
        {
            moveTowardsEnd = !moveTowardsEnd;
            distanceTraveled = 0.0f;
        }
    }
}