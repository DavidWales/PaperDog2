﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    public float rotateSpeed = 1.0f;
    public float rotateInterval = 10.0f;
    public Vector3 goalRotation;
    private float rTimer = 0.0f;
    private bool doRotation = false;
    private bool rotated = false;
    private Quaternion startRotation;
    private Quaternion endRotation;
    private Quaternion rotateTo;

    void Awake()
    {
        startRotation = this.transform.localRotation;
        endRotation = Quaternion.Euler(goalRotation.x, goalRotation.y, goalRotation.z);
    }

    void Update()
    {
        if (rTimer >= rotateInterval && !doRotation)
        {
            doRotation = true;
        }
        rTimer += Time.deltaTime;

        if (doRotation)
        {
            rotateTo = (rotated) ? startRotation : endRotation;
            this.transform.localRotation = Quaternion.RotateTowards(this.transform.localRotation, rotateTo, rotateSpeed * Time.deltaTime);
            if (Quaternion.Angle(this.transform.localRotation, rotateTo) <= 0.1f)
            {
                rotated = !rotated;
                rTimer = 0.0f;
                doRotation = false;
            }
        }
    }
}