﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryVariables
{
    public GameObject book;
    public AudioClip music;

    public StoryVariables(GameObject b, AudioClip m)
    {
        book = b;
        music = m;
    }
}