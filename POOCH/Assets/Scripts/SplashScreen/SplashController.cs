﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SplashController : MonoBehaviour
{
    public Sprite bookCover;
    public GameObject splashPages;
    public GameObject bookOrigin;

    [Serializable]
    public class ChapterToMenu
    {
        public string chapterName;
        public GameObject menu;
    }

    public ChapterToMenu[] chapterToMenu;
    private Dictionary<string, ChapterToMenu> menus = new Dictionary<string, ChapterToMenu>();

    public void OpenCover()
    {
        StartCoroutine(OpenBook());
    }

    IEnumerator OpenBook()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        AutoFlip a = GameObject.FindObjectOfType<AutoFlip>();
        Book book = a.GetComponent<Book>();
        book.bookPages[0] = bookCover;
        Destroy(a.transform.Find("StartButton").gameObject);
        a.FlipRightPage();
        yield return new WaitUntil(() => !a.pageFlipping());
        Destroy(a.gameObject);
        Instantiate(menus["LevelSelect"].menu, splashPages.transform.position, Quaternion.identity, splashPages.transform);
    }

    public void NextSplashPage(ButtonParams p)
    {
        BookManager.ReadBook(p.chapterName, null, SplashScreenTransitioned);
    }

    private void SplashScreenTransitioned(string chapterName)
    {
        Transform currPage = splashPages.transform.GetChild(0);
        Destroy(currPage.gameObject);
        Instantiate(menus[chapterName].menu, splashPages.transform.position, Quaternion.identity, splashPages.transform);
        DestroyBooks();
    }

    void Awake()
    {
        menus = chapterToMenu.ToDictionary(x => x.chapterName, x => x);
    }

    private void Start()
    {
        if (GlobalVariables.gameWasOpen)
        {
            DestroyBooks();
            Instantiate(menus["LevelSelect"].menu, splashPages.transform.position, Quaternion.identity, splashPages.transform);
        }
        else
        {
            GlobalVariables.gameWasOpen = true;
        }
    }

    private void DestroyBooks()
    {
        for (int i = 0; i < bookOrigin.transform.childCount; i++)
            Destroy(bookOrigin.transform.GetChild(i).gameObject);
    }
}
