﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonController : MonoBehaviour
{
    public Sprite completedSprite;
    public Sprite uncompletedSprite;
    public Sprite lockedSprite;
    public ButtonParams []requiredStories;
    private ButtonParams buttonParams;
    private Image buttonImage;

    void Start()
    {
        buttonImage = this.GetComponent<Image>();
        buttonParams = this.GetComponent<ButtonParams>();
        Sprite btnSpr = null;
        if (isAvailable())
        {
            if (GlobalVariables.levelsCompleted[buttonParams.chapterName])
            {
                btnSpr = completedSprite;
            }
            else
            {
                btnSpr = uncompletedSprite;
            }
        }
        else
        {
            btnSpr = lockedSprite;
            this.GetComponent<Button>().interactable = false;
        }
        buttonImage.sprite = btnSpr;
    }

    private bool isAvailable()
    {
        foreach(ButtonParams s in requiredStories)
        {
            if (!GlobalVariables.levelsCompleted[s.chapterName])
            {
                return false;
            }
        }
        return true;
    }
}