﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonParams : MonoBehaviour
{
    public string chapterName = "";
    public GameObject popup;

    public void ClickDown()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_DOWN);
    }

    public void ClickedUp()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        SplashController sc = GameObject.FindObjectOfType<SplashController>();
        sc.NextSplashPage(this);
    }

    public void StartStory()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        StoryReading storyReader = GameObject.FindObjectOfType<StoryReading>();
        storyReader.ReadStory(chapterName);
    }

    public void Popup()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        GameObject gObj = Instantiate(popup);
        gObj.transform.SetParent(transform.parent.parent, false);
        gObj.transform.localPosition = Vector2.zero;
    }
}