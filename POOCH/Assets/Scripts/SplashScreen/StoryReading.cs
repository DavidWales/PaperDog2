﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.SceneManagement;

public class StoryReading : MonoBehaviour
{
    [Serializable]
    public struct ChapterToLevel
    {
        public string ChapterName;
        public string LevelName;
        public AudioClip music;
    }

    public ChapterToLevel[] chapterToLevel;
    private Dictionary<string, ChapterToLevel> stories = new Dictionary<string, ChapterToLevel>();

    private void Awake()
    {
        stories = chapterToLevel.ToDictionary(x => x.ChapterName, x => x);
    }

    public void ReadStory(string chapterName)
    {
        BookManager.ReadBook(chapterName, StartLevelMusic, LoadNextLevel);
    }

    private void StartLevelMusic(string chapterName)
    {
        SoundManager.PlayMusic(stories[chapterName].music);
    }

    private void LoadNextLevel(string chapterName)
    {
        SceneManager.LoadScene(stories[chapterName].LevelName);
    }
}