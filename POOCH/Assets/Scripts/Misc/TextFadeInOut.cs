﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFadeInOut : MonoBehaviour
{
    public float FadeTime = 1.0f;
    private float _FadeTime = 0.0f;

    private Text _Text = null;
    private bool _Fade = false;
    private bool _FadeIn = true;

    public bool FadeOnStart = false;
    public bool FadeOutOnStart = true;
    public bool DisableOnComplete = false;

    void Start()
    {
        _Text = this.GetComponent<Text>();

        _Fade = FadeOnStart;
        if (FadeOutOnStart)
        {
            FadeOut();
        }
        else
        {
            FadeIn();
        }
    }

    void Update()
    {
        if (_Fade)
        {
            Color c = _Text.color;
            c.a = (_FadeTime / FadeTime);
            if (_FadeIn)
            {
                _FadeTime += Time.deltaTime;
                if (c.a >= 1.0f)
                {
                    _Fade = false;

                    if (DisableOnComplete)
                    {
                        this.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                _FadeTime -= Time.deltaTime;
                if (c.a <= 0.0f)
                {
                    _Fade = false;

                    if (DisableOnComplete)
                    {
                        this.gameObject.SetActive(false);
                    }
                }
            }
            _Text.color = c;
        }
    }

    public void FadeIn()
    {
        this.gameObject.SetActive(true);
        _Fade = true;
        _FadeIn = true;
        _FadeTime = 0.0f;
    }

    public void FadeOut()
    {
        this.gameObject.SetActive(true);
        _Fade = true;
        _FadeIn = false;
        _FadeTime = FadeTime;
    }

    public bool isTransitioning()
    {
        return _Fade;
    }
}