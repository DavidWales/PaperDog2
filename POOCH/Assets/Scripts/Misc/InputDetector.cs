﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputDetector
{
    private bool MobileInput(TouchPhase phase)
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == phase)
            {
                return true;
            }
        }
        return false;
    }

    private bool PCInput(Func<KeyCode, bool> detector)
    {
        return detector(GlobalVariables.pcButton);
    }

    //Return if input button was just pushed down
    public bool InputSensedDown()
    {
        return MobileInput(TouchPhase.Began) || PCInput(Input.GetKeyDown);
    }

    //Return if input button is down
    public bool InputSensed()
    {
        return Input.touchCount > 0 || Input.GetKey(GlobalVariables.pcButton);
    }

    public bool InputSensedUp()
    {
        return MobileInput(TouchPhase.Ended) || PCInput(Input.GetKeyUp);
    }
}