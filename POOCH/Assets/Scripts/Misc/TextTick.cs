﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextTick : MonoBehaviour
{
    public float tickTime = 3.0f;
    private float time = 0.0f;

    private Text dispText;

    void Awake()
    {
        dispText = this.GetComponent<Text>();
    }

    public void TickTo(int start, int final)
    {
        StopAllCoroutines();
        StartCoroutine(Tick(start, final));
    }

    private IEnumerator Tick(int start, int final)
    {
        dispText.text = start.ToString();
        time = 0.0f;
        while(true)
        {
            yield return null;
            time += Time.deltaTime;
            float perc = time / tickTime;
            perc = (perc > 1.0f) ? 1.0f : perc;
            int newText = (int)((final - start) * perc) + start;
            dispText.text = newText.ToString();
            if(newText == final)
            {
                break;
            }
        }
    }
}