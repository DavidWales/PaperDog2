﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedBook : MonoBehaviour
{
    public Sprite[] sprites;
    public float sprTime = 1.0f;
    private int image = 0;
    private Book book;
    private AutoFlip autoFlip;

    private void Awake()
    {
        book = this.GetComponent<Book>();
        autoFlip = this.GetComponent<AutoFlip>();
        StartCoroutine(Animate());
    }

    IEnumerator Animate()
    {
        while (true)
        {
            yield return new WaitForSeconds(sprTime);
            if (autoFlip.pageFlipping())
                break;
            image++;
            if (image >= sprites.Length)
                image = 0;
            book.bookPages[0] = sprites[image];
            book.UpdateSprites();
        }
    }
}