﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopOut : MonoBehaviour
{
    public Vector3 FinalScale;
    public float ScaleTime = 1.0f;

    void Start()
    {
        StartCoroutine(Pop());
    }

    private IEnumerator Pop()
    {
        float step = (FinalScale - this.transform.localScale).magnitude / ScaleTime;
        while(transform.localScale != FinalScale)
        {
            yield return null;
            transform.localScale = Vector3.MoveTowards(transform.localScale, FinalScale, step * Time.deltaTime);
        }
    }
}