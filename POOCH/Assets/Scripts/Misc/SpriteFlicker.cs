﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpriteFlicker : MonoBehaviour
{
    public float flickerTime = 1.0f;
    public float maxAlpha;
    public float minAlpha;
    public int flickers = 5;
    private SpriteRenderer r;
    private bool goingDown = true;
    private bool flickering = false;

    void Start()
    {
        r = this.GetComponent<SpriteRenderer>();
    }

    public void StartFlicker(Action callback)
    {
        if (flickering)
            StopAllCoroutines();
        StartCoroutine(Flicker(callback));
    }

    public bool isFlickering()
    {
        return flickering;
    }

    private IEnumerator Flicker(Action callback)
    {
        flickering = true;
        goingDown = true;
        int flicks = 0;
        float step = (maxAlpha - minAlpha) / flickerTime;
        while(flicks != flickers)
        {
            yield return null;
            Color c = r.color;
            if (goingDown)
            {
                c.a -= step * Time.deltaTime;
                if (c.a < minAlpha)
                {
                    c.a = minAlpha;
                    goingDown = !goingDown;
                }
            }
            else
            {
                c.a += step * Time.deltaTime;
                if (c.a > maxAlpha)
                {
                    c.a = maxAlpha;
                    goingDown = !goingDown;
                    flicks++;
                }
            }
            r.color = c;
        }
        flickering = false;
        callback();
    }
}