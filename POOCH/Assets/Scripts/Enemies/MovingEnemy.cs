﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemy : Enemy
{
    public float Speed = 1.0f;
    public Vector3 endPosition;
    private Vector3 startPosition;
    private bool moveTowardsEnd = true;

    void Start()
    {
        startPosition = this.transform.localPosition;
    }

    void Update()
    {
        Vector3 goal = (moveTowardsEnd) ? endPosition : startPosition;
        this.transform.position = Vector3.MoveTowards(this.transform.localPosition, goal, Speed * Time.deltaTime);
        if(Vector3.Distance(this.transform.localPosition, goal) <= 0.1f)
        {
            moveTowardsEnd = !moveTowardsEnd;
        }
    }
}