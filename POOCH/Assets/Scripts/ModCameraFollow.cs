﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModCameraFollow : MonoBehaviour
{
    public GameObject followObject;
    public Vector2 xBoundaries = new Vector2(0.0f, 0.0f);
    public Vector2 yBoundaries = new Vector2(0.1f, 0.9f);

    private Vector2 oldPosition;
    private bool followX = false;
    private bool followY = false;
    private Vector2 adjustVect = new Vector2(0.01f, 0.01f);

    private void Update()
    {
        followX = false;
        followY = false;
        Vector3 playerViewport = Camera.main.WorldToViewportPoint(followObject.transform.position);

        if (playerViewport.x > xBoundaries.y || playerViewport.x < xBoundaries.x)
        {
            followX = true;
        }

        if(playerViewport.y > yBoundaries.y || playerViewport.y < yBoundaries.x)
        {
            followY = true;
        }
    }

    private void LateUpdate()
    {
        if(!followX)
        {
            this.transform.position = new Vector3(oldPosition.x, this.transform.position.y, this.transform.position.z);
        }
        else
        {
            Vector2 dir = followObject.transform.position - this.transform.position;
            dir.Normalize();
            this.transform.position += new Vector3(0.0f, dir.y * adjustVect.y);
        }

        if (!followY)
        {
            this.transform.position = new Vector3(this.transform.position.x, oldPosition.y, this.transform.position.z);
        }
        else
        {
            Vector2 dir = followObject.transform.position - this.transform.position;
            dir.Normalize();
            this.transform.position += new Vector3(dir.x * adjustVect.x, 0.0f);
        }

        followX = false;
        followY = false;

        oldPosition = this.transform.position;
    }
}
