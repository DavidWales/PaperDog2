﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SallyController : MonoBehaviour
{
    public enum Goals
    {
        RUN_AWAY,
        WANDER
    }

    [System.Serializable]
    public class arg
    {
        public int intArg;
        public float floatArg;
        public bool boolArg;
    }

    public List<arg> arguments;
    public Goals Goal = Goals.RUN_AWAY;
    private SallyStrategy strategy;

    private void Awake()
    {
        SetStrategy(Goal);
    }

    private void Update()
    {
        strategy.Interact();
    }

    private void OnDestroy()
    {
        strategy.Destroyed();
    }

    public void SetStrategy(Goals g)
    {
        switch (g)
        {
            case Goals.RUN_AWAY:
                strategy = new SallyRun(this, arguments[0].floatArg, arguments[1].floatArg, arguments[2].boolArg);
                break;
            case Goals.WANDER:
                strategy = new SallyWander(this);
                break;
        }
        Goal = g;
        strategy.Activated();
    }
}