﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class SallyStrategy
{
    protected SallyController controller;

    public SallyStrategy(SallyController cont)
    {
        controller = cont;
    }

    abstract public void Interact();
    virtual public void Activated()
    {

    }

    virtual public void Destroyed()
    {

    }
}