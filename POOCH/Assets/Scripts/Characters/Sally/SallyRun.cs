﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SallyRun : SallyStrategy
{
    public float idleTime;
    private float runSpeed;
    private bool turn;

    private bool runningAway = false;
    private Animator animator;
    private GameObject goal;
    private Rigidbody2D contRb;

    public SallyRun(SallyController cont, float idle = 2.0f, float speed = 350.0f, bool t = true) : base(cont)
    {
        idleTime = idle;
        runSpeed = speed;
        turn = t;
    }

    public override void Activated()
    {
        base.Activated();
        animator = controller.GetComponent<Animator>();
        contRb = controller.GetComponent<Rigidbody2D>();
        goal = GameObject.Find("SallyGoal");
        controller.StartCoroutine(Wait());
        if(turn)
            controller.StartCoroutine(randomTurn());
    }

    private IEnumerator randomTurn()
    {
        while (!runningAway)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.75f, 1.5f));
            if (!runningAway)
            {
                controller.transform.localScale = new Vector3(-controller.transform.localScale.x, controller.transform.localScale.y);
            }
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(idleTime);
        runningAway = true;
        yield return new WaitForSeconds(2.0f); //Give sally a head start
        LevelManager.CreateTap();
        LevelManager.CanStart = true;
    }

    public override void Interact()
    {
        if (runningAway)
        {
            Vector3 dist = goal.transform.position - controller.transform.position;
            if (Mathf.Abs(dist.x) <= 0.1f)
            {
                GameObject.Destroy(controller.gameObject);
            }
            else
            {
                if(animator)
                    animator.SetBool("isRunning", true);
                int runDir = (dist.x >= 0.0f) ? 1 : -1;
                controller.transform.localScale = new Vector3(runDir, controller.transform.localScale.y);
                contRb.velocity = new Vector2(runSpeed * runDir * Time.deltaTime, contRb.velocity.y);
            }
        }
    }

    public override void Destroyed()
    {
        base.Destroyed();
        if (!LevelManager.CanStart)
        {
            LevelManager.CreateTap();
            LevelManager.CanStart = true;
        }
    }
}