﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SallyWander : SallyStrategy
{
    public SallyWander(SallyController cont) : base(cont) { }

    private Vector3 goalPosition;
    private Vector3 startPosition;
    private bool wandering = false;
    private float maxWander = 7.5f;
    private Animator animator;
    private float runSpeed = 150.0f;
    private Rigidbody2D contRb;

    public override void Activated()
    {
        base.Activated();
        startPosition = controller.transform.position;
        animator = controller.GetComponent<Animator>();
        contRb = controller.GetComponent<Rigidbody2D>();
        controller.StartCoroutine(Wander());
    }

    public override void Interact()
    {
        if(wandering && Mathf.Abs(controller.transform.position.x - goalPosition.x) < 0.1f)
        {
            wandering = false;
            contRb.velocity = Vector2.zero;
        }
    }

    private IEnumerator Wander()
    {
        while (true)
        {
            animator.SetBool("isRunning", false);
            yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 5.0f));
            controller.transform.localScale = new Vector3(-controller.transform.localScale.x, controller.transform.localScale.y);
            wandering = true;
            float newX = controller.transform.position.x + (UnityEngine.Random.Range(1.0f, 2.0f) * controller.transform.localScale.x);
            if(Mathf.Abs(newX - startPosition.x) > maxWander)
            {
                newX = startPosition.x + (maxWander * controller.transform.localScale.x);
            }
            goalPosition = new Vector3(newX, controller.transform.position.y);
            contRb.velocity = new Vector2(controller.transform.localScale.x * runSpeed * Time.deltaTime, contRb.velocity.y);
            animator.SetBool("isRunning", true);
            yield return new WaitUntil(() => wandering == false);
        }
    }
}