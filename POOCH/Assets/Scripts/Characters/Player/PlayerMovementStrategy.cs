﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class PlayerMovementStrategy
{
    protected PlayerController controller;

    public PlayerMovementStrategy(PlayerController playerController)
    {
        controller = playerController;
    }

    abstract public void Jump();
    abstract public void Run();
}