﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInactive : PlayerMovementStrategy
{
    private Rigidbody2D rb;

    public PlayerInactive(PlayerController playerController) : base(playerController)
    {
        rb = controller.GetComponent<Rigidbody2D>();
    }

    public override void Jump()
    {
        //Do Nothing
    }

    public override void Run()
    {
        //Do Nothing
        rb.velocity = Vector2.zero;
    }
}