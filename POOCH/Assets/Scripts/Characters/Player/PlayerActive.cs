﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActive : PlayerMovementStrategy
{
    private Rigidbody2D rb = null;
    private bool amJumping = false;
    private bool canJump = true;
    private float jumpTime = 0.0f;
    private InputDetector input = new InputDetector();
    private Animator animator;

    public PlayerActive(PlayerController playerController) : base(playerController)
    {
        rb = controller.GetComponent<Rigidbody2D>();
        animator = controller.GetComponent<Animator>();
    }

    public override void Jump()
    {
        bool jumpHeld = input.InputSensed();// JumpHeld();
        if (amJumping)
        {
            if (jumpTime <= controller.maxJumpTime)
            {
                if (jumpHeld)
                {
                    canJump = true;
                    Jump(controller.jumpForce * (jumpTime / controller.maxJumpTime));
                    jumpTime += Time.fixedDeltaTime;
                }
                else
                {
                    canJump = false;
                    jumpTime = controller.maxJumpTime + 1.0f;
                }
            }
            else
            {
                canJump = false;
            }
        }

        //Allow jumps if you landed on the ground
        bool grounded = controller.isGrounded();
        if (grounded && !canJump)
        {
            amJumping = false;
            jumpTime = 0.0f;
            canJump = true;
        }

        //If jump button pressed
        if (input.InputSensedDown() && !LevelManager.levelJustStarted() && grounded)
        {
            Jump(controller.jumpForce);
            SoundManager.PlaySound(SoundManager.SoundName.JUMP);
        }

    }

    public override void Run()
    {
        animator.SetBool("isWalking", true);
        rb.velocity = new Vector2(controller.runSpeed * controller.runDirection, rb.velocity.y);
    }

    //Perform jump
    private void Jump(float force)
    {
        if (canJump)
        {
            rb.velocity += new Vector2(0.0f, force * Time.deltaTime);
            if (rb.velocity.y > controller.maxJumpForce)
            {
                rb.velocity = new Vector2(rb.velocity.x, controller.maxJumpForce);
            }
            amJumping = true;
        }
    }
}