﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Run Variables
    public float runSpeed = 1.0f;
    public int runDirection = 1;

    //Misc
    private PlayerMovementStrategy movementStrategy;
    private float distToGround = 0.0f;

    //Jump Variables
    public float jumpForce = 1.0f;
    public float maxJumpForce = 3.0f;
    public float maxJumpTime = 1.0f;

    //Animations
    private Animator animator;

    public enum MovementStrategies
    {
        NORMAL,
        INACTIVE
    }

    void Awake()
    {
        distToGround = this.GetComponent<Collider2D>().bounds.extents.y;
        movementStrategy = new PlayerInactive(this);
        animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        movementStrategy.Jump();
        if (isGrounded())
        {
            animator.SetBool("isJumping", false);
        }
        else
        {
            animator.SetBool("isJumping", true);
        }
    }

    void FixedUpdate()
    {
        movementStrategy.Run();
    }

    public void ChangeDirection(int dir)
    {
        runDirection = dir;
        float newScale = (dir > 0) ? Mathf.Abs(this.transform.localScale.x) : -Mathf.Abs(this.transform.localScale.x);
        this.transform.localScale = new Vector3(newScale, this.transform.localScale.y, this.transform.localScale.z);
    }

    //Return if player is on the ground
    public bool isGrounded()
    {
        Vector2 topLeft = new Vector2(transform.position.x, transform.position.y) + (-Vector2.up * distToGround) + (-Vector2.right * (this.GetComponent<Collider2D>().bounds.extents.x - 0.5f));
        Vector2 bottomRight = new Vector2(transform.position.x, transform.position.y) + (-Vector2.up * (distToGround + 0.1f)) + (Vector2.right * (this.GetComponent<Collider2D>().bounds.extents.x - 0.5f));
        int oldLayer = this.gameObject.layer;
        this.gameObject.layer = 2;
        Collider2D areaCast = Physics2D.OverlapArea(topLeft, bottomRight);
        this.gameObject.layer = oldLayer;
        return areaCast;
    }

    public void SetMovementStrategy(MovementStrategies a)
    {
        switch (a)
        {
            case MovementStrategies.NORMAL:
                movementStrategy = new PlayerActive(this);
                break;
            case MovementStrategies.INACTIVE:
                movementStrategy = new PlayerInactive(this);
                break;
        }
        animator.SetBool("isWalking", false);
        animator.SetBool("isJumping", false);
    }
}
