﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public bool finalLevel = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            PlayerController player = GameObject.FindObjectOfType<PlayerController>();
            if (player)
            {
                player.SetMovementStrategy(PlayerController.MovementStrategies.INACTIVE);
            }
            GameManager.EndLevel(finalLevel);
        }
    }
}  