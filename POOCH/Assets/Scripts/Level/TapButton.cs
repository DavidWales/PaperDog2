﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapButton : MonoBehaviour
{
    public void OnClick()
    {
        LevelManager.TapDown();
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_DOWN);
    }

    public void OnRelease()
    {
        LevelManager.TapUp();
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
    }
}