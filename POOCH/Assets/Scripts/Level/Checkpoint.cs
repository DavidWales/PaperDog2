﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Sprite activeSprite;
    public Sprite inactiveSprite;
    public bool faceRight = true;
    private SpriteRenderer sprRender;

    private void Start()
    {
        sprRender = this.GetComponent<SpriteRenderer>();
        if (sprRender)
        {
            sprRender.sprite = inactiveSprite;
        }
    }

    public void SetSprite(bool active)
    {
        if (sprRender)
        {
            sprRender.sprite = (active) ? activeSprite : inactiveSprite;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            DeathManager.SetCheckpoint(this);
        }
    }
}