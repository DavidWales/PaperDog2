﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public int DeathPenalty = 25;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            DeathManager.Death(DeathPenalty);
            Animator playerAnimator = collision.GetComponent<Animator>();
            playerAnimator.SetBool("isDrowning", true);
            SoundManager.PlaySound(SoundManager.SoundName.SPLASH);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Animator playerAnimator = collision.GetComponent<Animator>();
            playerAnimator.SetBool("isDrowning", false);
        }
    }
}