﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ReportStats : MonoBehaviour
{
    public Text score;
    public float pointsTimer = 3.0f;
    public RawImage background;
    public Texture backgroundPressed;

    private float pTimer = 0.0f;
    private int scoreCollected;
    private int scoreStart;

    void Start()
    {
        scoreCollected = LevelManager.ScoreCollected;
        scoreStart = LevelManager.ScoreStart;

        score.text = scoreStart.ToString();
    }

    void Update()
    {
        int dispScore = Convert.ToInt32(score.text);
        float perc = pTimer / pointsTimer;
        perc = (perc > 1.0f) ? 1.0f : perc;
        int points = (int)(scoreCollected * perc) + scoreStart;
        if(dispScore != points)
        {
            SoundManager.PlaySound(SoundManager.SoundName.TICK);
        }
        score.text = points.ToString();
        pTimer += Time.deltaTime;
    }

    public void ChangeToPressed()
    {
        background.texture = backgroundPressed;
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_DOWN);
    }
}