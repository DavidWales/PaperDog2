﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public SettingsButton MusicButton;
    public SettingsButton EffectsButton;

    public void ToggleMusicVolume()
    {
        SoundManager.MusicOn = MusicButton.active;
    }

    public void ToggleEffectsVolume()
    {
        SoundManager.EffectsOn = EffectsButton.active;
    }

    public void ExitSettings()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        GameSaver.SaveVariables();
        ImageFadeInOut[] iFaders = transform.parent.GetComponentsInChildren<ImageFadeInOut>();
        foreach (ImageFadeInOut fader in iFaders)
        {
            fader.FadeOut();
        }

        TextFadeInOut[] tFaders = transform.parent.GetComponentsInChildren<TextFadeInOut>();
        foreach (TextFadeInOut fader in tFaders)
        {
            fader.FadeOut();
        }

        StartCoroutine(Destruct(iFaders, tFaders));
    }

    private bool AllFaded(ImageFadeInOut[] iFaders, TextFadeInOut[] tFaders)
    {
        foreach (ImageFadeInOut fader in iFaders)
        {
            if (fader.GetComponent<Image>().color.a > 0.0f)
            {
                return false;
            }
        }

        foreach (TextFadeInOut fader in tFaders)
        {
            if (fader.GetComponent<Text>().color.a > 0.0f)
            {
                return false;
            }
        }

        return true;
    }

    private IEnumerator Destruct(ImageFadeInOut[] iFaders, TextFadeInOut[] tFaders)
    {
        yield return new WaitUntil(() => AllFaded(iFaders, tFaders));
        Destroy(this.transform.parent.gameObject);
    }
}