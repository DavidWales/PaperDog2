﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsButton : MonoBehaviour
{
    public Sprite OnNormalSprite;
    public Sprite OnPushedSprite;
    public Sprite OffNormalSprite;
    public Sprite OffPushedSprite;
    private Image image;
    private Button button;
    public bool active = false;

    public enum Setting
    {
        MUSIC,
        EFFECTS
    }
    public Setting setting;

    void Awake()
    {
        image = this.GetComponent<Image>();
        button = this.GetComponent<Button>();
    }

    void Start()
    {
        active = true;
        switch (setting)
        {
            case Setting.MUSIC:
                active = SoundManager.MusicOn;
                break;
            case Setting.EFFECTS:
                active = SoundManager.EffectsOn;
                break;
        }

        SetSprites(active);
    }
    private void SetSprites(bool state)
    {
        SpriteState ss = button.spriteState;
        if (state)
        {
            image.sprite = OnNormalSprite;
            ss.highlightedSprite = OnPushedSprite;
            ss.pressedSprite = OnPushedSprite;
        }
        else
        {
            image.sprite = OffNormalSprite;
            ss.highlightedSprite = OffPushedSprite;
            ss.pressedSprite = OffPushedSprite;
        }
        button.spriteState = ss;
        active = state;
    }

    public void Toggle()
    {
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        SetSprites(!active);
    }
}