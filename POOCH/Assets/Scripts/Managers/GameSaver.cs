﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameSaver : MonoBehaviour
{
    public int numLevels = 6;
    private string levelKey = "Level_";
    private string scoreKey = "Score";
    private string musicKey = "SoundMusicOn";
    private string effectsKey = "SoundEffectsOn";
    private string itemKey = "Item_";

    void Start()
    {
        List<string> sceneNames = new List<string>();
        for(int i = 0; i < numLevels; i++)
        {
            sceneNames.Add(levelKey + i);
        }

        if(sceneNames.Count > 0)
        {
            Dictionary<string, bool> levelsCompleted = new Dictionary<string, bool>();
            foreach (string s in sceneNames)
            {
                bool levelSuccess = false; //Defeat level completion to false
                if(PlayerPrefs.HasKey(levelKey + s))
                    levelSuccess = (PlayerPrefs.GetInt(levelKey + s) == 1) ? true : false;
                levelsCompleted.Add(s, levelSuccess);
            }
            GlobalVariables.levelsCompleted = levelsCompleted;
        }

        int score = 0; //Default score to 0
        if (PlayerPrefs.HasKey(scoreKey))
            score = PlayerPrefs.GetInt(scoreKey);
        ScoreManager.SetScore(score);

        bool musicOn = true; //Default music to on
        if(PlayerPrefs.HasKey(musicKey))
            musicOn = PlayerPrefs.GetInt(musicKey) == 0 ? false : true;
        SoundManager.MusicOn = musicOn;

        bool effectsOn = true; //Default effects to on
        if(PlayerPrefs.HasKey(effectsKey))
            effectsOn = PlayerPrefs.GetInt(effectsKey) == 0 ? false : true;
        SoundManager.EffectsOn = effectsOn;
    }

    void OnApplicationQuit()
    {
        SaveVariables();
    }

    void OnApplicationPause(bool pause)
    {
        if (pause)
            SaveVariables();
    }

    public static void GetItem(string itemName)
    {
        if (!GlobalVariables.unlockedItems.ContainsKey(itemName))
        {
            bool hasItem = (PlayerPrefs.GetInt(_inst.itemKey + itemName) == 1) ? true : false;
            GlobalVariables.unlockedItems.Add(itemName, hasItem);
        }
    }

    public static void SaveVariables()
    {
        foreach (KeyValuePair<string, bool> level in GlobalVariables.levelsCompleted)
        {
            PlayerPrefs.SetInt(_inst.levelKey + level.Key, Convert.ToInt16(level.Value));
        }
        PlayerPrefs.SetInt(_inst.scoreKey, ScoreManager.GetScore());
        PlayerPrefs.SetInt(_inst.musicKey, (SoundManager.MusicOn == true) ? 1 : 0);
        PlayerPrefs.SetInt(_inst.effectsKey, (SoundManager.EffectsOn == true) ? 1 : 0);

        foreach (KeyValuePair<string, bool> item in GlobalVariables.unlockedItems)
        {
            PlayerPrefs.SetInt(_inst.itemKey + item.Key, (item.Value == true) ? 1 : 0);
        }
    }

    //Singleton Stuff
    private static GameSaver _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public static void CreateManager()
    {
#if UNITY_EDITOR
        if (_inst == null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/GameSaver.prefab", typeof(GameObject)) as GameObject;
            Instantiate(prefab);
        }
#endif
    }
}