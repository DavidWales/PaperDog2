﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathManager : MonoBehaviour
{
    public ImageFadeInOut screenFade;

    public GameObject InitialCheckpoint;
    private Checkpoint latestCheckpoint = null;

    private bool respawning = false;
    private bool moved = false;
    private PlayerController player;

    public static void Death(int penalty)
    {
        if (_inst.respawning == false)
        {
            LevelManager.Deaths += 1;
            _inst.player.SetMovementStrategy(PlayerController.MovementStrategies.INACTIVE);
            ScoreManager.ChangeScore(-penalty);
            _inst.respawning = true;
            _inst.moved = false;
            _inst.screenFade.FadeIn();
        }
    }

    public static void Hurt(int penalty)
    {
        SpriteFlicker playerFlicker = _inst.player.GetComponent<SpriteFlicker>();
        if (_inst.respawning == false && !playerFlicker.isFlickering())
        {
            ScoreManager.ChangeScore(-penalty);
            playerFlicker.StartFlicker(_inst.DoneFlickering);
            Physics2D.IgnoreLayerCollision(_inst.player.gameObject.layer, LayerMask.NameToLayer("Enemy"), true);
        }
    }

    private void DoneFlickering()
    {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), false);
    }

    public static void SetCheckpoint(Checkpoint cp)
    {
        SoundManager.PlaySound(SoundManager.SoundName.CHECKPOINT_ACTIVE);
        if(_inst.latestCheckpoint != null)
        {
            _inst.latestCheckpoint.SetSprite(false);
        }
        _inst.latestCheckpoint = cp;
        _inst.latestCheckpoint.SetSprite(true);
    }

    private void Update()
    {
        if (respawning)
        {
            if (!moved && !screenFade.isTransitioning())
            {
                player.transform.position = new Vector3(latestCheckpoint.transform.position.x, latestCheckpoint.transform.position.y, player.transform.position.z);
                player.ChangeDirection((latestCheckpoint.faceRight) ? 1 : -1);
                moved = true;
                screenFade.FadeOut();
            }
            if (moved && !screenFade.isTransitioning())
            {
                Respawn();
                respawning = false;
            }
        }
    }

    private void Respawn()
    {
        player.SetMovementStrategy(PlayerController.MovementStrategies.NORMAL);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        if (player)
        {
            GameObject iCheckpoint = Instantiate(InitialCheckpoint, player.transform.position, Quaternion.identity);
            latestCheckpoint = iCheckpoint.GetComponent<Checkpoint>();
        }
    }

    //Singleton Stuff
    private static DeathManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
    }
}