﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthManager : MonoBehaviour
{
    public int startingLives = 5;
    public GameObject healthPrefab;
    public Vector2 healthSpacing = new Vector2(-55.0f, -55.0f);
    public int maxLivesPerRow = 5;
    private int lives;
    private List<GameObject> healthImages = new List<GameObject>();
    private GameObject healthLoc = null;

    public static void DecrementLives()
    {
        _inst.lives--;
        _inst.RemoveHeart();
    }

    public static void IncrementLives()
    {
        _inst.lives++;
        int healthCount = _inst.healthImages.Count;
        if(healthCount > 0)
        {
            _inst.AddHeart(_inst.healthImages[healthCount - 1]);
        }
        else
        {
            _inst.AddHeart(_inst.healthLoc);
        }
    }

    public static int Lives
    {
        get { return _inst.lives; }
        set { _inst.lives = value; }
    }

    public static void ResetLives()
    {
        _inst.lives = _inst.startingLives;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        healthImages.Clear();
        healthLoc = GameObject.Find("HealthLoc");
        if (healthLoc)
        {
            for(int i = 0; i < lives; i++)
            {
                if(i > 0)
                {
                    AddHeart(healthImages[i - 1]);
                }
                else
                {
                    AddHeart(healthLoc);
                }
            }
        }
    }

    private void AddHeart(GameObject lastHealth)
    {
        Vector2 pos = lastHealth.transform.position;
        int healthCount = healthImages.Count;
        if(healthCount == 0)
        {
            pos = lastHealth.transform.position;
        }
        else
        {
            if (healthCount % maxLivesPerRow == 0)
            {
                pos = new Vector2(healthLoc.transform.position.x, pos.y + healthSpacing.y);
            }
            else
            {
                pos += new Vector2(healthSpacing.x, 0.0f);
            }
        }
        GameObject newHealth = Instantiate(healthPrefab, pos, Quaternion.identity, healthLoc.transform);
        healthImages.Add(newHealth);
    }

    private void RemoveHeart()
    {
        if (healthImages.Count > 0)
        {
            GameObject lastHeart = healthImages[healthImages.Count - 1];
            healthImages.Remove(lastHeart);
            Destroy(lastHeart);
        }
    }

    //Singleton Stuff
    private static HealthManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        lives = startingLives;
    }
}