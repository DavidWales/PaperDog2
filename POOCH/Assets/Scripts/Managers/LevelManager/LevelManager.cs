﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : SceneVariables
{
    public GameObject TapButton;
    public Sprite TapPressed;
    private Image TapImage;

    private bool levelStarted = false;
    private PlayerController player = null;
    private float levelTime = 0.0f;
    private InputDetector input = new InputDetector();

    public bool canLoseScore = false;
    public static bool CanLoseScore
    {
        get { return _inst.canLoseScore; }
        set { _inst.canLoseScore = value; }
    }

    private int levelDeaths = 0;
    public static int Deaths
    {
        get { return _inst.levelDeaths; }
        set { _inst.levelDeaths = value; }
    }

    private int scoreCollected = 0;
    public static int ScoreCollected
    {
        get { return _inst.scoreCollected; }
        set { _inst.scoreCollected = value; }
    }

    private int scoreStart = 0;
    public static int ScoreStart
    {
        get { return _inst.scoreStart; }
        set { _inst.scoreStart = value; }
    }

    public static void CreateTap()
    {
        GameObject loc = GameObject.Find("TapLoc");
        if (loc)
        {
            _inst.TapImage = Instantiate(_inst.TapButton, 
                                         loc.transform.position, 
                                         Quaternion.identity, 
                                         loc.transform).transform.Find("TapImage").GetComponent<Image>();
        }
    }

    public static void TapDown()
    {
        if (_inst.canStart)
        {
            _inst.TapImage.sprite = _inst.TapPressed;
        }
    }

    public static void TapUp()
    {
        if (_inst.canStart)
        {
            if (_inst.player)
            {
                _inst.player.SetMovementStrategy(PlayerController.MovementStrategies.NORMAL);
                _inst.levelStarted = true;
            }
            if (_inst.TapImage)
            {
                Destroy(_inst.TapImage.transform.parent.gameObject);
            }
        }
    }

    [SerializeField]
    private bool canStart = true;
    public static bool CanStart
    {
        get { return _inst.canStart; }
        set { _inst.canStart = value; }
    }

    private void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        if (player)
        {
            player.SetMovementStrategy(PlayerController.MovementStrategies.INACTIVE);
        }
        scoreStart = ScoreManager.GetScore();
        
        if (canStart)
        {
            CreateTap();
        }
    }

    private void Update()
    {
        if(levelStarted)
        {
            _inst.levelTime += Time.deltaTime;
        }
        if (!levelStarted && canStart)
        {
            if (input.InputSensedDown())
            {
                
            }
        }
        else
        {
            _inst.levelTime += Time.deltaTime;
        }
    }

    public static bool levelJustStarted()
    {
        return _inst.levelTime <= 0.5f && _inst.levelStarted;
    }

    //Singleton Stuff
    private static LevelManager _inst = null;
    override protected void Awake()
    {
        base.Awake();
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
    }
}