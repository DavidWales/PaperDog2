﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneVariables : MonoBehaviour
{
    public AudioClip music;
    public bool forceMusicStart = false;

    virtual protected void Awake()
    {
        if (GameObject.FindObjectOfType<TipManager>() == null)
            TipManager.CreateManager();
        if (GameObject.FindObjectOfType<SoundManager>() == null)
            SoundManager.CreateManager();
        if (GameObject.FindObjectOfType<ScoreManager>() == null)
            ScoreManager.CreateManager();
        if (GameObject.FindObjectOfType<GameManager>() == null)
            GameManager.CreateManager();
        if (GameObject.FindObjectOfType<GameSaver>() == null)
            GameSaver.CreateManager();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        if (forceMusicStart)
        {
            SoundManager.PlayMusic(music);
        }
    }
}