﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TipManager : MonoBehaviour
{
    public enum TipName
    {
        JUMP,
        LONG_JUMP,
        WALL_GRAPPLE,
        START_LEVEL
    }

    [System.Serializable]
    public struct Tip
    {
        public TipName tipType;
        public string tipText; 
    }
    public Tip[] Tips;
    private Dictionary<TipName, string> tipDict;
    private bool tipDisplayed = false;
    private GameObject TipObj;
    private Text tText;
    private InputDetector input = new InputDetector();

    public static void DisplayTip(TipName t)
    {
        if(_inst.tipDict.ContainsKey(t))
        {
            string text = _inst.tipDict[t];
            _inst.tText.text = text;
            _inst.TipObj.SetActive(true);
            _inst.tipDisplayed = true;
            Time.timeScale = 0.0f;
        }
    }

    void Update()
    {
        if (tipDisplayed)
        {
            if (input.InputSensedDown())
            {
                if (TipObj)
                {
                    TipObj.SetActive(false);
                    Time.timeScale = 1.0f;
                    tipDisplayed = false;
                }
            }
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        TipObj = GameObject.Find("Tips");
        if (TipObj)
        {
            GameObject textObj = TipObj.transform.Find("TipText").gameObject;
            if (textObj)
            {
                tText = textObj.GetComponent<Text>();
            }
            TipObj.SetActive(false);
        }
    }

    //Singleton Stuff
    private static TipManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        tipDict = Tips.ToDictionary(x => x.tipType, x => x.tipText);
    }

    public static void CreateManager()
    {
#if UNITY_EDITOR
        if (_inst == null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/TipManager.prefab", typeof(GameObject)) as GameObject;
            Instantiate(prefab);
        }
#endif
    }
}