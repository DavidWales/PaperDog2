﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : MonoBehaviour
{
    public GameObject statsDisplayPrefab;
    private InputDetector input = new InputDetector();

    public static void EndLevel(bool wasFinalLevel)
    {
        _inst.StartCoroutine(_inst.LevelComplete(wasFinalLevel));
    }

    private IEnumerator LevelComplete(bool wasFinalLevel)
    {
        //Display stats on screen
        GameObject statsLoc = GameObject.Find("StatsLoc");
        ReportStats statsScreen = null;
        if (statsLoc)
        {
            statsScreen = Instantiate(_inst.statsDisplayPrefab, statsLoc.transform.position, Quaternion.identity, statsLoc.transform).GetComponent<ReportStats>();
        }

        //Log level progress
        string sceneName = SceneManager.GetActiveScene().name;
        if (GlobalVariables.levelsCompleted.ContainsKey(sceneName))
            GlobalVariables.levelsCompleted[sceneName] = true;
        else
            GlobalVariables.levelsCompleted.Add(sceneName, true);

        //Wait until the user indicates they're done looking at their stats
        yield return new WaitUntil(() => input.InputSensedDown());

        if (statsScreen)
            statsScreen.ChangeToPressed();
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_DOWN);

        yield return new WaitUntil(() => input.InputSensedUp());
        //Return to level select screen if not last chapter, otherwise show conclusion book
        SoundManager.PlaySound(SoundManager.SoundName.BUTTON_CLICK_UP);
        if (!wasFinalLevel)
            BookManager.ReadBook("LevelSelect", null, ReadingComplete);
        else
            BookManager.ReadBook("Final_Chapter", null, ReadingComplete);
    }

    private void ReadingComplete(string chapterName)
    {
        GameSaver.SaveVariables();
        SceneManager.LoadScene("SplashScreen");
    }

    //Singleton Stuff
    private static GameManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public static void CreateManager()
    {
#if UNITY_EDITOR
        if (_inst == null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/GameManager.prefab", typeof(GameObject)) as GameObject;
            Instantiate(prefab);
        }
#endif
    }
}
