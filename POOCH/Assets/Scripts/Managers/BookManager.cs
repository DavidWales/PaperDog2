﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BookManager : MonoBehaviour
{
    public GameObject bookPrefab;
    public Sprite pageBack;
    private InputDetector input = new InputDetector();

    [Serializable]
    public class Chapter
    {
        public string chapterName;
        public Sprite[] pageSprites;
        public Sprite destination_image;
    }

    public class ChapterInfo
    {
        public ChapterInfo(Sprite[] sprites, Sprite dSprite)
        {
            pageSprites = sprites;
            destination_image = dSprite;
    }
        public Sprite[] pageSprites;
        public Sprite destination_image;
    }

    public Chapter[] chaptersData;
    private Dictionary<string, ChapterInfo> chapters = new Dictionary<string, ChapterInfo>();

    public static void ReadBook(string name, Action<string> bookCreated = null, Action<string> bookRead = null)
    {
        _inst.StartCoroutine(_inst.Read(name, bookCreated, bookRead));
    }

    private IEnumerator Read(string name, Action<string> bookCreated, Action<string> bookRead)
    {
        yield return new WaitForEndOfFrame();
        //Create book
        GameObject book = ConstructBook(name);
        Book b = book.GetComponent<Book>();
        AutoFlip a = book.GetComponent<AutoFlip>();
        int numPages = (int)Mathf.Ceil(b.bookPages.Length / 2.0f) - 1;

        //Callback to indicate that book was created
        if (bookCreated != null)
            bookCreated(name);

        yield return null; //Wait for book to be created fully (getting current screen may take a frame)
        a.FlipRightPage();

        yield return new WaitUntil(() => !a.pageFlipping()); //Wait until automatic page is done flipping
        int currentPage = 1;
        if (currentPage < numPages)
        {
            //Flip through pages until you reach the end
            while (true)
            {
                if(input.InputSensedDown() && !a.pageFlipping())
                {
                    a.FlipRightPage();
                    currentPage++;
                    if (currentPage >= numPages)
                    {
                        //End of book, exit when page is done flipping
                        yield return new WaitUntil(() => !a.pageFlipping());
                        break;
                    }
                }
                yield return null;
            }
        }

        //Callback to indicate that the book has been read
        if (bookRead != null)
            bookRead(name);
    }

    private GameObject ConstructBook(string name)
    {
        //Ensure the chapter exists and 
        Debug.Assert(chapters.ContainsKey(name));
        GameObject bookOrigin = GameObject.Find("BookOrigin");
        Debug.Assert(bookOrigin != null);
        GameObject book = GameObject.Instantiate(bookPrefab, bookOrigin.transform.position, Quaternion.identity, bookOrigin.transform);

        //Get sprite of current page
        Texture2D tex = new Texture2D(Screen.width, Screen.height);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();
        Sprite currPageImg = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));

        Book b = book.GetComponent<Book>();
        b.canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        ChapterInfo cInfo = chapters[name];
        int numPages = 3 + cInfo.pageSprites.Length * 2; //Front/back page for each page, and then front only for last page
        Sprite[] pages = new Sprite[numPages];
        pages[0] = currPageImg;
        pages[1] = pageBack;
        for(int i = 0; i < cInfo.pageSprites.Length; i++)
        {
            int index = (i + 1) * 2;
            pages[index] = cInfo.pageSprites[i];
            pages[index + 1] = pageBack;
        }
        pages[pages.Length - 1] = cInfo.destination_image;
        b.bookPages = pages;
        return book;
    }

    //Singleton Stuff
    private static BookManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        chapters = chaptersData.ToDictionary(x => x.chapterName, x => new ChapterInfo(x.pageSprites, x.destination_image));
    }
}