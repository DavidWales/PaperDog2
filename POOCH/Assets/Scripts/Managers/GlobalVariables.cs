﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables
{
    //////////////////EDIT THESE VARIABLES TO MODIFY THE GAME//////////////////
    public static KeyCode pcButton = KeyCode.W;


    //////////////////BELOW THIS LINE IS FOR RUNTIME VARIABLES (EDIT AT YOUR OWN PERIL)//////////////////
    public static bool gameWasOpen = false;
    public static Dictionary<string, bool> levelsCompleted = new Dictionary<string, bool>();
    public static Dictionary<string, bool> unlockedItems = new Dictionary<string, bool>();
}