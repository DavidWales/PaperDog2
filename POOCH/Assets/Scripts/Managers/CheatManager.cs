﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatManager : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            //GameManager.ChangeLevel("Prototype");
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            TipManager.DisplayTip(TipManager.TipName.WALL_GRAPPLE);
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            ScreenCapture.CaptureScreenshot("LatestScreenShot.png");
        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            Time.timeScale = (Time.timeScale == 0.0f) ? 1.0f : 0.0f;
        }
        else if (Input.GetKeyDown(KeyCode.F5))
        {
            ScoreManager.ChangeScore(5000);
        }
    }

    //Singleton Stuff
    private static CheatManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
}