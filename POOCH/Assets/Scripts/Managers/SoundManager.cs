﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SoundManager : MonoBehaviour
{
    public float musicFadeTime = 5.0f;

    private float maxMusicVolume = 1.0f;
    //private float maxEffectsVolume = 1.0f;

    private bool musicOn = true;
    public static bool MusicOn
    {
        get { return _inst.musicOn; }
        set { _inst.music_audio.volume = (value == true) ? 1.0f : 0.0f; _inst.maxMusicVolume = _inst.music_audio.volume; _inst.musicOn = value; }
    }

    private bool effectsOn = true;
    public static bool EffectsOn
    {
        get { return _inst.effectsOn; }
        set { _inst.effects_audio.volume = (value == true) ? 1.0f : 0.0f; /*_inst.maxEffectsVolume = _inst.effects_audio.volume;*/ _inst.effectsOn = value; }
    }

    private enum MusicMode
    {
        FADING_IN,
        FADING_OUT,
        NORMAL
    }
    private MusicMode mode = MusicMode.NORMAL;

    public enum SoundName
    {
        JUMP,
        PAGE_TURN,
        TICK,
        CHECKPOINT_ACTIVE,
        SCORE_COLLECTED,
        SPLASH,
        BUTTON_CLICK_DOWN,
        BUTTON_CLICK_UP
    }

    [Serializable]
    public struct Sound
    {
        public SoundName name;
        public AudioClip[] clips;
    }

    public Sound[] Sounds;
    private Dictionary<SoundName, AudioClip[]> soundDict = new Dictionary<SoundName, AudioClip[]>();

    public AudioSource music_audio;
    public AudioSource effects_audio;
    private AudioClip queuedMusic = null;

    public static void PlaySound(SoundName s)
    {
        //if (_inst.soundDict[s].Length <= 0)
         //   return;

        System.Random rand = new System.Random();
        int index = rand.Next(0, _inst.soundDict[s].Length);
        _inst.effects_audio.PlayOneShot(_inst.soundDict[s][index]);
    }

    public static void PlaySound(string s)
    {
        SoundName soundName = (SoundName)Enum.Parse(typeof(SoundName), s);
        Debug.Log(soundName.ToString());
        PlaySound(soundName);
    }

    public static void PlayMusic(AudioClip music)
    {
        if (_inst.music_audio.clip == music && _inst.mode != MusicMode.FADING_OUT)
            return;
        if (_inst.mode == MusicMode.FADING_IN)
            _inst.StopCoroutine(_inst.FadeMusic());
        _inst.queuedMusic = music;
        _inst.StartCoroutine(_inst.FadeMusic());
    }

    private IEnumerator FadeMusic()
    {
        float timeStep = maxMusicVolume / musicFadeTime;

        if (music_audio.isPlaying)
        {
            mode = MusicMode.FADING_OUT;
            while (music_audio.volume > 0.0f)
            {
                yield return null;
                music_audio.volume -= timeStep * Time.deltaTime;
                music_audio.volume = (music_audio.volume < 0.0f) ? 0.0f : music_audio.volume;
            }
        }

        music_audio.clip = queuedMusic;
        music_audio.Play();

        mode = MusicMode.FADING_IN;
        while (music_audio.volume < maxMusicVolume)
        {
            yield return null;
            music_audio.volume += timeStep * Time.deltaTime;
            music_audio.volume = (music_audio.volume > maxMusicVolume) ? maxMusicVolume : music_audio.volume;
        }

        mode = MusicMode.NORMAL;
    }

    //Singleton Stuff
    private static SoundManager _inst = null;
    void Awake()
    {
        if (_inst == null)
        {
            _inst = this;
        }
        else if (_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        soundDict = Sounds.ToDictionary(x => x.name, x => x.clips);
    }

    public static void CreateManager()
    {
#if UNITY_EDITOR
        if (_inst == null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/SoundManager.prefab", typeof(GameObject)) as GameObject;
            Instantiate(prefab);
        }
#endif
    }
}