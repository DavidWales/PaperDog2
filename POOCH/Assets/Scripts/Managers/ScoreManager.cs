﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ScoreManager : MonoBehaviour
{
    private int score = 0;

    public static void ChangeScore(int change)
    {
        int newScore = _inst.score + change;
        if(newScore < LevelManager.ScoreStart && !LevelManager.CanLoseScore)
        {
            change = LevelManager.ScoreStart - _inst.score;
            newScore = LevelManager.ScoreStart;
        }
        _inst.score = newScore;
        LevelManager.ScoreCollected += change;
        _inst.UpdateUI();
    }

    public static int GetScore()
    {
        return _inst.score;
    }

    public static void SetScore(int newScore)
    {
        _inst.score = newScore;
    }

    private void UpdateUI()
    {
        GameObject scoreGObj = GameObject.Find("Score");
        if (scoreGObj != null)
        {
            Text scoreUI = scoreGObj.GetComponent<Text>();
            scoreUI.text = LevelManager.ScoreCollected.ToString();
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        UpdateUI();
    }

    //Singleton Stuff
    private static ScoreManager _inst = null;
    void Awake()
    {
        if(_inst == null)
        {
            _inst = this;
        }
        else if(_inst != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public static void CreateManager()
    {
#if UNITY_EDITOR
        if (_inst == null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/ScoreManager.prefab", typeof(GameObject)) as GameObject;
            Instantiate(prefab);
        }
#endif
    }
}
